﻿using Julas.Utils.Properties;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Julas.Utils")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Julas.Utils")]
[assembly: AssemblyCopyright("Copyright © Julian Kwieciński 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("dfc3fa4e-949b-4284-b302-9699b9d34c89")]
[assembly: AssemblyVersion(AssemblyInfo.VersionNumber)]
[assembly: AssemblyFileVersion(AssemblyInfo.VersionNumber)]
[assembly: AssemblyInformationalVersion(AssemblyInfo.VersionNumber)]
[assembly: InternalsVisibleTo("Julas.Utils.UnitTest")]

namespace Julas.Utils.Properties
{
    internal static class AssemblyInfo
    {
        internal const string VersionNumber = "2.0.5.0";
    }
}