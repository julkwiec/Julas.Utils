﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Julas.Utils.Collections
{
    public static class CartesianProduct
    {
        public static IEnumerable<Tuple<T1, T2>> Cartesian<T1, T2>(this
            IEnumerable<T1> collection1,
            IEnumerable<T2> collection2)
        {
            foreach (var i1 in collection1)
                foreach (var i2 in collection2)
                    yield return Tuple.Create(i1, i2);
        }

        public static IEnumerable<Tuple<T1, T2, T3>> Cartesian<T1, T2, T3>(this
            IEnumerable<T1> collection1,
            IEnumerable<T2> collection2,
            IEnumerable<T3> collection3)
        {
            foreach (var i1 in collection1)
                foreach (var i2 in collection2)
                    foreach (var i3 in collection3)
                        yield return Tuple.Create(i1, i2, i3);
        }

        public static IEnumerable<Tuple<T1, T2, T3, T4>> Cartesian<T1, T2, T3, T4>(this
            IEnumerable<T1> collection1,
            IEnumerable<T2> collection2,
            IEnumerable<T3> collection3,
            IEnumerable<T4> collection4)
        {
            foreach (var i1 in collection1)
                foreach (var i2 in collection2)
                    foreach (var i3 in collection3)
                        foreach (var i4 in collection4)
                            yield return Tuple.Create(i1, i2, i3, i4);
        }

        public static IEnumerable<Tuple<T1, T2, T3, T4, T5>> Cartesian<T1, T2, T3, T4, T5>(this
            IEnumerable<T1> collection1,
            IEnumerable<T2> collection2,
            IEnumerable<T3> collection3,
            IEnumerable<T4> collection4,
            IEnumerable<T5> collection5)
        {
            foreach (var i1 in collection1)
                foreach (var i2 in collection2)
                    foreach (var i3 in collection3)
                        foreach (var i4 in collection4)
                            foreach (var i5 in collection5)
                                yield return Tuple.Create(i1, i2, i3, i4, i5);
        }

        public static IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> Cartesian<T1, T2, T3, T4, T5, T6>(this
            IEnumerable<T1> collection1,
            IEnumerable<T2> collection2,
            IEnumerable<T3> collection3,
            IEnumerable<T4> collection4,
            IEnumerable<T5> collection5,
            IEnumerable<T6> collection6)
        {
            foreach (var i1 in collection1)
                foreach (var i2 in collection2)
                    foreach (var i3 in collection3)
                        foreach (var i4 in collection4)
                            foreach (var i5 in collection5)
                                foreach (var i6 in collection6)
                                    yield return Tuple.Create(i1, i2, i3, i4, i5, i6);
        }

        public static IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> Cartesian<T1, T2, T3, T4, T5, T6, T7>(this
            IEnumerable<T1> collection1,
            IEnumerable<T2> collection2,
            IEnumerable<T3> collection3,
            IEnumerable<T4> collection4,
            IEnumerable<T5> collection5,
            IEnumerable<T6> collection6,
            IEnumerable<T7> collection7)
        {
            foreach (var i1 in collection1)
                foreach (var i2 in collection2)
                    foreach (var i3 in collection3)
                        foreach (var i4 in collection4)
                            foreach (var i5 in collection5)
                                foreach (var i6 in collection6)
                                    foreach (var i7 in collection7)
                                        yield return Tuple.Create(i1, i2, i3, i4, i5, i6, i7);
        }

        public static IEnumerable<IEnumerable<T>> Cartesian<T>(this IEnumerable<IEnumerable<T>> collections)
        {
            IEnumerable<IEnumerable<T>> empty = new[] { Enumerable.Empty<T>() };
            return collections.Aggregate(empty,
                (accumulator, sequence) =>
                    accumulator.SelectMany(accseq =>
                        sequence, (accseq, item) =>
                            accseq.Concat(new[] { item })));
        }
    }
}