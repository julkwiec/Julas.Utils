﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Julas.Utils.Collections
{
    public static class CollectionExtensionMethods
    {
        public static bool IsNullOrEmpty(this IEnumerable collection)
        {
            if (collection == null) return true;
            if (collection.GetEnumerator().MoveNext()) return false;
            return true;
        }

        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection) action(item);
        }

        public static IEnumerable<T> Convert<T>(this IEnumerable collection)
        {
            return collection.Cast<object>().Select(item => item.ConvertTo<T>());
        }

        public static IEnumerable<T> Convert<T>(this IEnumerable collection, T defaultValue)
        {
            return collection.Cast<object>().Select(item => item.ConvertTo<T>(defaultValue));
        }

        public static IEnumerable<T> ConvertFun<T>(this IEnumerable collection, Func<T> defaultValueGenerator)
        {
            return collection.Cast<object>().Select(item => item.ConvertToFun<T>(defaultValueGenerator));
        }

        public static IEnumerable<T> Repeat<T>(this IEnumerable<T> collection)
        {
            while (true)
                foreach (var item in collection)
                    yield return item;
        }

        public static IEnumerable<T> Repeat<T>(this IEnumerable<T> collection, int repeatCount)
        {
            if (repeatCount < 0) throw new ArgumentOutOfRangeException(nameof(repeatCount));
            for (int i = 0; i < repeatCount; i++)
            {
                foreach (var item in collection) yield return item;
            }
        }

        public static bool ContainsAnyOf<T>(this IEnumerable<T> collection, IEnumerable<T> elements)
        {
            return collection.Any(elements.Contains);
        }

        public static bool ContainsAnyOf<T>(this IEnumerable<T> collection, IEnumerable<T> elements,
            IEqualityComparer<T> comparer)
        {
            return collection.Any(i => elements.Contains(i, comparer));
        }

        public static bool ContainsAnyOf<T>(this IEnumerable<T> collection, params T[] elements)
        {
            return collection.ContainsAnyOf((IEnumerable<T>)elements);
        }

        public static IEnumerable<T> Append<T>(this IEnumerable<T> collection, params T[] elements)
        {
            return collection.Concat(elements);
        }

        public static IEnumerable<T> Except<T>(this IEnumerable<T> collection, params T[] elements)
        {
            return collection.Except(elements.AsEnumerable());
        } 
    }
}