﻿using System;
using System.Collections.Generic;

namespace Julas.Utils
{
    public static class EqualityComparer
    {
        public static IEqualityComparer<T> Create<T>(Func<T, T, bool> comparer, Func<T, int> getHashCode)
        {
            return new GenericComparer<T>(comparer, getHashCode);
        }

        public static IEqualityComparer<T> Create<T>(Func<T, T, bool> comparer)
        {
            return new GenericComparer<T>(comparer);
        }

        public static IEqualityComparer<T> Create<T>()
        {
            return new GenericComparer<T>();
        }
    }

    internal class GenericComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> _Comparer;
        private readonly Func<T, int> _GetHashCode;

        public GenericComparer()
        {
        }

        public GenericComparer(Func<T, T, bool> comparer)
        {
            _Comparer = comparer;
        }

        public GenericComparer(Func<T, T, bool> comparer, Func<T, int> getHashCode)
        {
            _Comparer = comparer;
            _GetHashCode = getHashCode;
        }

        public bool Equals(T x, T y)
        {
            return _Comparer?.Invoke(x, y) ?? (object.Equals(x, y));
        }

        public int GetHashCode(T obj)
        {
            return _GetHashCode?.Invoke(obj) ?? obj.GetHashCode();
        }
    }
}