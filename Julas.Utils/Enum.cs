﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Julas.Utils
{
    public static class Enum<T> where T: struct
    {
        public static IEnumerable<T> GetValues()
        {
            return Enum.GetValues(typeof (T)).Cast<T>();
        } 
    }
}
