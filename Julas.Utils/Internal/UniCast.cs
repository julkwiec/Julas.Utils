﻿using System;
using System.Linq;
using System.Reflection;

namespace Julas.Utils.Internal
{
    internal static class UniCast
    {
        public static object ChangeType(object item, Type destinationType)
        {
            var unpacked = Unpack(item);
            var packed = Pack(unpacked, destinationType);
            return packed;
        }

        private static object Unpack(object item)
        {
            if (item == null || item == DBNull.Value) return null;
            var itemType = item.GetType();
            if (itemType.IsClass) return item;

            var tmpItem = new CastItem(item, itemType);
            tmpItem = TryUnpackNullable(tmpItem);
            tmpItem = TryUnpackEnum(tmpItem);
            return tmpItem.Value;
        }

        private static CastItem TryUnpackEnum(CastItem item)
        {
            if (!item.Type.IsEnum) return item;
            var type = Enum.GetUnderlyingType(item.Type);
            var value = Convert.ChangeType(item.Value, type);
            return new CastItem(value, type);
        }

        private static CastItem TryUnpackNullable(CastItem item)
        {
            var type = Nullable.GetUnderlyingType(item.Type);
            if (type == null) return item;
            var value = Convert.ChangeType(item.Value, type);
            return new CastItem(value, type);
        }

        private static object Pack(object item, Type type)
        {
            if (item == null || item == DBNull.Value) return null;
            if (type.IsClass) return item;
            var underlyingNullableType = Nullable.GetUnderlyingType(type);
            var isNullable = underlyingNullableType != null;
            var isEnum = isNullable ? underlyingNullableType.IsEnum : type.IsEnum;

            if (isEnum)
            {
                if (isNullable)
                {
                    var enumUnderlyingType = Enum.GetUnderlyingType(underlyingNullableType);
                    var tmp = ConvertType(item, enumUnderlyingType);
                    tmp = Enum.ToObject(underlyingNullableType, tmp);
                    return tmp;
                }
                else
                {
                    var enumUnderlyingType = Enum.GetUnderlyingType(type);
                    var tmp = ConvertType(item, enumUnderlyingType);
                    tmp = Enum.ToObject(type, tmp);
                    return tmp;
                }
            }
            else
            {
                if (isNullable)
                {
                    var tmp = ConvertType(item, underlyingNullableType);
                    return tmp;
                }
                else
                {
                    var tmp = ConvertType(item, type);
                    return tmp;
                }
            }
        }

        private static object ConvertType(object item, Type type)
        {
            if (type.IsInstanceOfType(item))
            {
                return item;
            }
            try
            {
                if (item is IConvertible)
                {
                    return Convert.ChangeType(item, type);
                }
            }
            catch (Exception)
            {
            }
            return ReflectionCast(item, type);
        }

        private static object ReflectionCast(object item, Type type)
        {
            return item.GetType()
                .GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic |
                                BindingFlags.FlattenHierarchy)
                .Where(m => m.Name == "op_Implicit" || m.Name == "op_Explicit")
                .First(m => m.ReturnType == type)
                .Invoke(item, null);
        }

        private struct CastItem
        {
            public object Value { get; }
            public Type Type { get; }

            public CastItem(object value, Type type)
            {
                Value = value;
                Type = type;
            }
        }
    }
}