﻿using System;
using System.Threading;

namespace Julas.Utils
{
    public abstract class DisposableBase : IDisposable
    {
        private int _IsDisposed;

        protected bool IsDisposed => Interlocked.CompareExchange(ref _IsDisposed, 0, 0) == 1;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        public void Dispose()
        {
            if (Interlocked.CompareExchange(ref _IsDisposed, 1, 0) == 0)
            {
                GC.SuppressFinalize(this);
                Dispose(true);
            }
        }

        protected void ThrowIfDisposed()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        protected void ThrowIfDisposed(string message)
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name, message);
            }
        }

        protected abstract void Dispose(bool disposing);

        ~DisposableBase()
        {
            Dispose(false);
        }
    }
}