﻿using System;

namespace Julas.Utils
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Option<T>
    {
        private readonly T _Value;
        public bool HasValue { get; }

        public T Value
        {
            get
            {
                if (HasValue) return _Value;
                throw new InvalidOperationException("Value is not present");
            }
        }

        public Option(T value)
        {
            HasValue = value != null;
            _Value = value;
        }

        public TRet Map<TRet>(Func<T, TRet> some, Func<TRet> none)
        {
            return HasValue ? some(_Value) : none();
        }

        public Option<TRet> Map<TRet>(Func<T, TRet> some)
        {
            return HasValue ? some(_Value) : Option<TRet>.None;
        }

        public Option<T> Do(Action<T> some, Action none)
        {
            if (HasValue) some(_Value);
            else none();
            return this;
        }

        public static Option<T> None => new Option<T>();

        public static explicit operator T(Option<T> item)
        {
            return item.Value;
        }

        public static implicit operator Option<T>(T item)
        {
            return new Option<T>(item);
        }

        public bool Equals(Option<T> obj)
        {
            if (obj.HasValue && this.HasValue) return obj.Value.Equals(this.Value);
            return obj.HasValue == this.HasValue;
        }

        public override bool Equals(object obj)
        {
            var obj2 = obj as Option<T>?;
            return obj2.HasValue && Equals(obj2.Value);
        }

        public static bool operator ==(Option<T> left, Option<T> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Option<T> left, Option<T> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return HasValue ? Value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return HasValue ? Value.ToString() : "None";
        }
    }

    public static class Option
    {
        public static Option<T> Some<T>(T value)
        {
            if (value == null) throw new InvalidOperationException("Value is not present");
            return new Option<T>(value);
        }
    }
}