﻿using System;

namespace Julas.Utils
{
    public static class DelegateExtensionMethods
    {
        public static void Raise(this EventHandler handler, object sender, EventArgs e)
        {
            handler?.Invoke(sender, e);
        }

        public static void Raise<T>(this EventHandler<T> handler, object sender, T e)
        {
            handler?.Invoke(sender, e);
        }

        public static void Raise(this Action action)
        {
            action?.Invoke();
        }

        public static void Raise<T1>(this Action<T1> action, T1 arg1)
        {
            action?.Invoke(arg1);
        }

        public static void Raise<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2)
        {
            action?.Invoke(arg1, arg2);
        }

        public static void Raise<T1, T2, T3>(this Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
        {
            action?.Invoke(arg1, arg2, arg3);
        }

        public static void Raise<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            action?.Invoke(arg1, arg2, arg3, arg4);
        }

        public static void Raise<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> action, T1 arg1, T2 arg2, T3 arg3,
            T4 arg4, T5 arg5)
        {
            action?.Invoke(arg1, arg2, arg3, arg4, arg5);
        }

        public static void Raise<T1, T2, T3, T4, T5, T6>(this Action<T1, T2, T3, T4, T5, T6> action, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            action?.Invoke(arg1, arg2, arg3, arg4, arg5, arg6);
        }

        public static void Raise<T1, T2, T3, T4, T5, T6, T7>(this Action<T1, T2, T3, T4, T5, T6, T7> action, T1 arg1,
            T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
            action?.Invoke(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
        }

        public static void Raise<T1, T2, T3, T4, T5, T6, T7, T8>(this Action<T1, T2, T3, T4, T5, T6, T7, T8> action,
            T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
        {
            action?.Invoke(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
        }
    }
}