using System;
using System.Reactive.Disposables;

namespace Julas.Utils.Connections
{
    public interface IConnectionSource<T> : ICancelable, IObservable<IConnection<T>>
    {
    }
}