using System;
using System.Reactive;
using System.Threading.Tasks;
using Julas.Utils.Extensions;
using Julas.Utils.Streams;

namespace Julas.Utils.Connections
{
    internal class StreamConnectionBase<T> : DisposableBase, IConnection<T>
    {
        private readonly IObservable<T> _MessageSource;
        private readonly IObjectReader<T> _Reader;
        private readonly IObjectWriter<T> _Writer;

        public StreamConnectionBase(IStreamInterface stream, Func<IStreamInterface, Task<Option<T>>> read, Func<IStreamInterface, T, Task<Unit>> write)
        {
            _Reader = ObjectReader.Create(stream, read);
            _Writer = ObjectWriter.Create(stream, write);
            _MessageSource = _Reader.ToObservable();
        }

        public StreamConnectionBase(IObjectReader<T> reader, IObjectWriter<T> writer)
        {
            _Reader = reader;
            _Writer = writer;
            _MessageSource = _Reader.ToObservable();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            ThrowIfDisposed();
            return _MessageSource.Subscribe(observer);
        }

        public void Send(T message)
        {
            _Writer.Write(message);
        }

        public async Task<Unit> SendAsync(T message)
        {
            ThrowIfDisposed();
            return await _Writer.WriteAsync(message);
        }

        protected override void DoDispose()
        {
            _Reader.Dispose();
            _Writer.Dispose();
        }
    }
}