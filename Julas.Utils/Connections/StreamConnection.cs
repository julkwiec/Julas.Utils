using Julas.Utils.Streams;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reactive;
using System.Threading.Tasks;
using Julas.Utils.Datagrams;

namespace Julas.Utils.Connections
{
    public static class StreamConnection
    {
        public static IConnection<T> Create<T>(IStreamInterface stream, Func<IStreamInterface, Task<Option<T>>> read, Func<IStreamInterface, T, Task<Unit>> write)
        {
            return new StreamConnectionBase<T>(stream, read, write);
        }

        public static IConnection<T> Create<T>(IObjectReader<T> reader, IObjectWriter<T> writer)
        {
            return new StreamConnectionBase<T>(reader, writer);
        }
    }

    public static class DatagramConnection
    {
        public static IConnection<T> Create<T>(IDatagramInterface datagram,
            Func<IDatagramInterface, Task<Option<T>>> read, Func<IDatagramInterface, T, Task<Unit>> write)
        {
            return new DatagramConnectionBase<T>(datagram, read, write);
        }
        

    }

    public class DatagramConnectionBase<T> : DisposableBase, IConnection<T>
    {
        public DatagramConnectionBase(IDatagramInterface datagram, Func<IDatagramInterface, Task<Option<T>>> read, Func<IDatagramInterface, T, Task<Unit>> write)
        {
            throw new NotImplementedException();
        }
    }
}