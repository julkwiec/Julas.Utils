﻿using Julas.Utils.Sockets;
using System;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Linq;

namespace Julas.Utils.Connections
{
    internal class TcpConnectionSource<T> : DisposableBase, IConnectionSource<T>
    {
        private readonly IObservable<IConnection<T>> _ConnectionSource;
        private readonly TcpListener _Listener;

        public TcpConnectionSource(IPEndPoint listeningEndPoint, Func<TcpClient, IConnection<T>> connectionFactory)
        {
            _Listener = new TcpListener(listeningEndPoint);
            _Listener.Start();
            _ConnectionSource = _Listener
                .ToObservable()
                .Select(connectionFactory);
        }

        public ICancelable Subscribe(IObserver<IConnection<T>> observer)
        {
            ThrowIfDisposed();
            return _ConnectionSource.Subscribe(observer);
        }

        protected override void DoDispose()
        {
            _Listener.Stop();
        }
    }

    public static class TcpConnectionSource
    {
        public static IConnectionSource<T> Create<T>(IPEndPoint listeningEndPoint, Func<TcpClient, IConnection<T>> connectionFactory)
        {
            return new TcpConnectionSource<T>(listeningEndPoint, connectionFactory);
        }
    }
}