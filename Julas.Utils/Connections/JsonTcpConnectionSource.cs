﻿using System;
using System.Net;

namespace Julas.Utils.Connections
{
    internal class JsonTcpConnectionSource<T> : DisposableBase, IConnectionSource<T>
    {
        private readonly IConnectionSource<T> _ConnectionSource;

        public JsonTcpConnectionSource(IPEndPoint listeningEndPoint)
        {
            _ConnectionSource = TcpConnectionSource.Create<T>(listeningEndPoint, JsonTcpConnection.Create<T>);
        }

        protected override void DoDispose()
        {
            _ConnectionSource.Dispose();
        }

        public ICancelable Subscribe(IObserver<IConnection<T>> observer)
        {
            return _ConnectionSource.Subscribe(observer);
        }
    }

    public static class JsonTcpConnectionSource
    {
        public static IConnectionSource<T> Create<T>(IPEndPoint listeningEndPoint)
        {
            return new JsonTcpConnectionSource<T>(listeningEndPoint);
        }
    }
}