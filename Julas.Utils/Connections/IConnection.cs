﻿using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;

namespace Julas.Utils.Connections
{
    public interface IConnection<T> : ICancelable, IObservable<T>
    {
        void Send(T message);

        Task<Unit> SendAsync(T message);
    }
}