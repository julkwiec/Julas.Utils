using Julas.Utils.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reactive;
using System.Threading.Tasks;

namespace Julas.Utils.Connections
{
    /// <summary>
    /// Packet format is length:body where length is an ASCII represented number of bytes forming the body.
    ///
    /// </summary>
    internal class JsonTcpConnection<T> : DisposableBase, IConnection<T>
    {
        private const char HeaderSeparator = ':';
        private readonly IConnection<T> _Connection;
        private readonly JsonSerializer _Serializer;

        public JsonTcpConnection(TcpClient client)
        {
            _Serializer = new JsonSerializer();
            _Connection = StreamConnection.Create<T>(client, Read, Write);
        }

        protected override void DoDispose()
        {
            _Connection.Dispose();
        }

        private async Task<Option<T>> Read(Streams stream)
        {
            var bodyLength = await ReadHeader(stream);
            if (bodyLength < 0) return Option<T>.None;
            var body = await ReadBody(stream, bodyLength);
            using (var memoryStream = new MemoryStream(body))
            using (var streamReader = new StreamReader(memoryStream))
            using (var jsonReader = new JsonTextReader(streamReader))
            {
                return _Serializer.Deserialize<T>(jsonReader);
            }
        }

        private async Task<byte[]> ReadBody(Streams stream, int length)
        {
            byte[] body = new byte[length];
            int bytesRead = await stream.ReadAsync(body, 0, length);
            if (bytesRead < length) throw new FormatException("Unexpected end of stream.");
            return body;
        }

        private async Task<int> ReadHeader(Streams stream)
        {
            List<char> chars = new List<char>();
            while (true)
            {
                int headerByte = await stream.ReadByteAsync();
                if (headerByte < 0)
                {
                    if (chars.Any()) throw new ArgumentException("Unexpected end of stream");
                    else return -1;
                }
                char headerChar = (char)headerByte;
                if (Char.IsDigit(headerChar)) chars.Add(headerChar);
                else if (headerChar == HeaderSeparator) break;
                else throw new FormatException("Invalid character in header: " + headerChar);
            }
            return chars.JoinChars().Map(Int32.Parse);
        }

        private async Task<Unit> Write(Streams str, T msg)
        {
            byte[] data = null;
            using (var tmpStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(tmpStream))
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                _Serializer.Serialize(jsonWriter, msg);
                jsonWriter.Flush();
                data = tmpStream.ToArray();
            }
            var header = CreateHeader(data.Length);
            await str.WriteAsync(header, 0, header.Length);
            await str.WriteAsync(data, 0, data.Length);
            return Unit.Default;
        }

        private byte[] CreateHeader(int length)
        {
            return String.Format("{0}{1}", length, HeaderSeparator).EncodeUTF8();
        }

        public ICancelable Subscribe(IObserver<T> observer)
        {
            return _Connection.Subscribe(observer);
        }

        public void Send(T message)
        {
            _Connection.Send(message);
        }

        public async Task<Unit> SendAsync(T message)
        {
            return await _Connection.SendAsync(message);
        }
    }

    public static class JsonTcpConnection
    {
        public static IConnection<T> Create<T>(TcpClient client)
        {
            return new JsonTcpConnection<T>(client);
        }

        public static IConnection<T> Create<T>(IPEndPoint endPoint)
        {
            return new JsonTcpConnection<T>(new TcpClient().Do(c => c.Connect(endPoint)));
        }
    }
}