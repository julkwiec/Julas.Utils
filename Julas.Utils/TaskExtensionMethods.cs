﻿using System;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace Julas.Utils.Extensions
{
    public static class TaskExtensionMethods
    {
        public static T WaitThrow<T>(this Task<T> task)
        {
            try
            {
                task.Wait();
                return task.Result;
            }
            catch (AggregateException ex)
            {
                ExceptionDispatchInfo.Capture(ex.InnerException).Throw();
                return default(T);
            }
        }

        public static void WaitThrow(this Task task)
        {
            try
            {
                task.Wait();
            }
            catch (AggregateException ex)
            {
                ExceptionDispatchInfo.Capture(ex.InnerException).Throw();
            }
        }
    }
}