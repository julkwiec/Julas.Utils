﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Julas.Utils.MathTools
{
    public struct Interval : IEquatable<Interval>
    {
        public int LowerBound { get; }
        public int UpperBound { get; }

        public int Count => UpperBound - LowerBound + 1;

        public Interval(int bound1, int bound2)
        {
            LowerBound = Math.Min(bound1, bound2);
            UpperBound = Math.Max(bound1, bound2);
        }

        public bool Contains(int value)
        {
            return value >= LowerBound && value <= UpperBound;
        }

        public bool Contains(Interval other)
        {
            return other.LowerBound >= LowerBound && other.UpperBound <= UpperBound;
        }

        public bool Overlaps(Interval other)
        {
            if (other.LowerBound < LowerBound && other.UpperBound < LowerBound) return false;
            if (other.LowerBound > UpperBound && other.UpperBound > UpperBound) return false;
            return true;
        }

        public IEnumerable<int> ToEnumerable()
        {
            return Enumerable.Range(LowerBound, Count);
        }

        public override int GetHashCode()
        {
            return LowerBound.GetHashCode() ^ UpperBound.GetHashCode();
        }

        public bool Equals(Interval other)
        {
            return other.LowerBound == LowerBound && other.UpperBound == UpperBound;
        }

        public override bool Equals(object obj)
        {
            if (obj is Interval) return Equals((Interval)obj);
            return false;
        }

        public static bool operator ==(Interval left, Interval right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Interval left, Interval right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return $"{{{LowerBound},{UpperBound}}}";
        }
    }
}