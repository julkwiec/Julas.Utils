﻿using System;

namespace Julas.Utils.MathTools
{
    public static class MathExtensionMethods
    {
        public static int DivideRoundUp(this int number, int divisor)
        {
            if (divisor == 0) throw new DivideByZeroException();
            if (number == 0) return 0;

            var sign = ((number > 0 && divisor > 0) || (number < 0 && divisor < 0))
                ? 1
                : -1;

            var absNumber = Math.Abs(number);
            var absDivisor = Math.Abs(divisor);
            return sign * ((absNumber + absDivisor - 1) / absDivisor);
        }
    }
}