﻿using System;

namespace Julas.Utils.MathTools
{
    public struct CircularInteger
    {
        public Interval Interval { get; }
        public int Value { get; }

        public CircularInteger(Interval interval, int value)
        {
            Interval = interval;
            Value = CalculateValue(interval, value);
        }

        private static int CalculateValue(Interval interval, int value)
        {
            var offset = interval.LowerBound;
            var tmpMax = interval.UpperBound - offset;
            var count = interval.Count;
            var tmpValue = value - offset;

            if (tmpValue < 0)
                tmpValue = tmpMax - Math.Abs(tmpValue % count) + 1;
            else
                tmpValue = tmpValue % count;
            return tmpValue + offset;
        }

        public int DistanceTo(int value)
        {
            return GetDistance(Interval, Value, value);
        }

        public static int GetDistance(Interval interval, int value1, int value2)
        {
            var point1 = CalculateValue(interval, value1);
            var point2 = CalculateValue(interval, value2);
            var distance = Math.Abs(point1 - point2);
            return Math.Min(distance, interval.Count - distance);
        }

        public static CircularInteger operator +(CircularInteger left, int right)
        {
            return new CircularInteger(left.Interval, left.Value + right);
        }

        public static CircularInteger operator ++(CircularInteger item)
        {
            return new CircularInteger(item.Interval, item.Value + 1);
        }

        public static CircularInteger operator -(CircularInteger left, int right)
        {
            return new CircularInteger(left.Interval, left.Value - right);
        }

        public static CircularInteger operator --(CircularInteger item)
        {
            return new CircularInteger(item.Interval, item.Value - 1);
        }

        public static CircularInteger operator *(CircularInteger left, int right)
        {
            return new CircularInteger(left.Interval, left.Value * right);
        }

        public static CircularInteger operator /(CircularInteger left, int right)
        {
            return new CircularInteger(left.Interval, left.Value / right);
        }

        public static implicit operator int(CircularInteger item)
        {
            return item.Value;
        }

        public override string ToString()
        {
            return $"Value: {Value}, range: {Interval}";
        }
    }
}