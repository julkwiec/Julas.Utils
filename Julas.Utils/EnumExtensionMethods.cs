﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Julas.Utils
{
    public static class EnumExtensionMethods
    {
        public static bool HasFlags(this Enum value, IEnumerable<Enum> flags)
        {
            return flags.All(value.HasFlag);
        }

        public static bool HasFlags(this Enum value, params Enum[] flags)
        {
            return value.HasFlags(flags.AsEnumerable());
        }

        public static bool HasAnyFlag(this Enum value, IEnumerable<Enum> flags)
        {
            return flags.Any(value.HasFlag);
        }

        public static bool HasAnyFlag(this Enum value, params Enum[] flags)
        {
            return value.HasAnyFlag(flags.AsEnumerable());
        }
    }
}