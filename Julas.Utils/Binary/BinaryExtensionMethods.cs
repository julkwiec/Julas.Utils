﻿using System;
using System.Collections.Generic;

namespace Julas.Utils.Binary
{
    public static class BinaryExtensionMethods
    {
        /// <summary>
        /// </summary>
        /// <param name="b"></param>
        /// <param name="bitIndex">0 - MSB, 7 - LSB</param>
        /// <returns></returns>
        public static bool ReadBit(this byte b, int bitIndex)
        {
            if (bitIndex < 0 || bitIndex > 7) throw new ArgumentOutOfRangeException(nameof(bitIndex));
            return ((0x80 >> bitIndex) & b) > 0;
        }

        #region WriteBytes

        public static byte[] WriteBytes(this byte[] array, int offset, ArraySegment<byte> bytes)
        {
            Array.Copy(bytes.Array, bytes.Offset, array, offset, bytes.Count);
            return array;
        }

        public static byte[] WriteBytes(this byte[] array, int offset, byte[] bytes)
        {
            Array.Copy(bytes, 0, array, 0, array.Length);
            return array;
        }

        public static byte[] WriteBytes(this byte[] array, int offset, IEnumerable<byte> bytes)
        {
            foreach (var b in bytes)
            {
                array[offset] = b;
                offset++;
            }
            return array;
        }

        public static ArraySegment<byte> WriteBytes(this ArraySegment<byte> array, int offset, ArraySegment<byte> bytes)
        {
            Array.Copy(bytes.Array, bytes.Offset, array.Array, array.Offset + offset, bytes.Count);
            return array;
        }

        public static ArraySegment<byte> WriteBytes(this ArraySegment<byte> array, int offset, byte[] bytes)
        {
            Array.Copy(bytes, 0, array.Array, array.Offset + offset, bytes.Length);
            return array;
        }

        public static ArraySegment<byte> WriteBytes(this ArraySegment<byte> array, int offset, IEnumerable<byte> bytes)
        {
            offset += array.Offset;
            foreach (var b in bytes)
            {
                array.Array[offset] = b;
                offset++;
            }
            return array;
        }

        #endregion WriteBytes

        #region ReadBytes

        public static byte[] ReadBytes(this byte[] array, int offset, int count)
        {
            byte[] ret = new byte[count];
            Array.Copy(array, offset, ret, 0, count);
            return ret;
        }

        public static byte[] ReadBytes(this ArraySegment<byte> array, int offset, int count)
        {
            byte[] ret = new byte[count];
            Array.Copy(array.Array, array.Offset + offset, ret, 0, count);
            return ret;
        }

        public static ArraySegment<byte> GetSegment(this byte[] array, int offset, int count)
        {
            return new ArraySegment<byte>(array, offset, count);
        }

        public static ArraySegment<byte> GetSegment(this ArraySegment<byte> array, int offset, int count)
        {
            return new ArraySegment<byte>(array.Array, array.Offset + offset, count);
        }

        #endregion ReadBytes

        #region Byte

        public static byte[] WriteByte(this byte[] array, int index, byte value)
        {
            array[index] = value;
            return array;
        }

        public static ArraySegment<byte> WriteByte(this ArraySegment<byte> array, int index, byte value)
        {
            array.Array[array.Offset + index] = value;
            return array;
        }

        public static byte ReadByte(this byte[] array, int index)
        {
            return array[index];
        }

        public static byte ReadByte(this ArraySegment<byte> array, int index)
        {
            return array.Array[array.Offset + index];
        }

        #endregion Byte

        #region Word

        public static ArraySegment<byte> WriteNWord(this ArraySegment<byte> array, int index, ushort value)
        {
            for (int i = 0; i < sizeof(ushort); i++)
            {
                array.Array[array.Offset + index + i] = unchecked((byte)(value >> (8 * (sizeof(ushort) - 1 - i))));
            }
            return array;
        }

        public static byte[] WriteNWord(this byte[] array, int index, ushort value)
        {
            return WriteNWord(new ArraySegment<byte>(array), index, value).Array;
        }

        public static ushort ReadNWord(this ArraySegment<byte> array, int index)
        {
            ushort ret = 0;
            for (int i = 0; i < sizeof(ushort); i++)
            {
                ret = (ushort)((ret << 8) + array.Array[array.Offset + index + i]);
            }
            return ret;
        }

        public static ushort ReadNWord(this byte[] array, int index)
        {
            return ReadNWord(new ArraySegment<byte>(array), index);
        }

        #endregion Word

        #region Dword

        public static ArraySegment<byte> WriteNDword(this ArraySegment<byte> array, int index, uint value)
        {
            for (int i = 0; i < sizeof(uint); i++)
            {
                array.Array[array.Offset + index + i] = unchecked((byte)(value >> (8 * (sizeof(uint) - 1 - i))));
            }
            return array;
        }

        public static byte[] WriteNDword(this byte[] array, int index, uint value)
        {
            return WriteNDword(new ArraySegment<byte>(array), index, value).Array;
        }

        public static uint ReadNDword(this ArraySegment<byte> array, int index)
        {
            uint ret = 0;
            for (int i = 0; i < sizeof(uint); i++)
            {
                ret = (uint)((ret << 8) + array.Array[array.Offset + index + i]);
            }
            return ret;
        }

        public static uint ReadNDword(this byte[] array, int index)
        {
            return ReadNDword(new ArraySegment<byte>(array), index);
        }

        #endregion Dword

        #region Qword

        public static ArraySegment<byte> WriteNQword(this ArraySegment<byte> array, int index, ulong value)
        {
            for (int i = 0; i < sizeof(ulong); i++)
            {
                array.Array[array.Offset + index + i] = unchecked((byte)(value >> (8 * (sizeof(ulong) - 1 - i))));
            }
            return array;
        }

        public static byte[] WriteNQword(this byte[] array, int index, ulong value)
        {
            return WriteNQword(new ArraySegment<byte>(array), index, value).Array;
        }

        public static ulong ReadNQword(this ArraySegment<byte> array, int index)
        {
            ulong ret = 0;
            for (int i = 0; i < sizeof(ulong); i++)
            {
                ret = (ulong)((ret << 8) + array.Array[array.Offset + index + i]);
            }
            return ret;
        }

        public static ulong ReadNQword(this byte[] array, int index)
        {
            return ReadNQword(new ArraySegment<byte>(array), index);
        }

        #endregion Qword
    }
}