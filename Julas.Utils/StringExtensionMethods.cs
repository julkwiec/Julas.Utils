﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Julas.Utils.Extensions
{
    public static class StringExtensionMethods
    {
        public static string Decode(this byte[] data, Encoding encoding)
        {
            return encoding.GetString(data);
        }

        public static string DecodeUTF8(this byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }

        public static string Decode(this ArraySegment<byte> data, Encoding encoding)
        {
            return encoding.GetString(data.Array, data.Offset, data.Count);
        }

        public static string DecodeUTF8(this ArraySegment<byte> data)
        {
            return Encoding.UTF8.GetString(data.Array, data.Offset, data.Count);
        }

        public static byte[] Encode(this string str, Encoding encoding)
        {
            return encoding.GetBytes(str);
        }

        public static byte[] EncodeUTF8(this string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return String.IsNullOrEmpty(str);
        }

        public static bool IsNullOrWhitespace(this string str)
        {
            return String.IsNullOrWhiteSpace(str);
        }

        public static string IfEmptyThenNull(this string str)
        {
            return String.IsNullOrEmpty(str) ? null : str;
        }

        public static string IfEmptyOrWhitespaceThenNull(this string str)
        {
            return String.IsNullOrWhiteSpace(str) ? null : str;
        }

        public static string IfNullThenEmpty(this string str)
        {
            return str ?? String.Empty;
        }

        public static string IfNullThen(this string str, string value)
        {
            return str ?? value;
        }

        public static string IfNullOrEmptyThen(this string str, string value)
        {
            return String.IsNullOrEmpty(str) ? value : str;
        }

        public static string IfNullOrWhitespaceThen(this string str, string value)
        {
            return String.IsNullOrWhiteSpace(str) ? value : str;
        }

        public static byte[] GetBytes(this string str, Encoding encoding)
        {
            return encoding.GetBytes(str);
        }

        public static string Join(this IEnumerable<string> coll, string separator)
        {
            separator = separator.IfNullThenEmpty();
            var sb = new StringBuilder();
            var first = true;
            foreach (var str in coll)
            {
                if (!first) sb.Append(separator);
                first = false;
                sb.Append(str);
            }
            return sb.ToString();
        }

        public static string JoinChars(this IEnumerable<char> chars)
        {
            return new string(chars.ToArray());
        }

        public static string ToStringDef(this object obj, string nullValueString)
        {
            return obj?.ToString() ?? nullValueString;
        }

        public static bool IsRegexMatch(this string str, string pattern)
        {
            return Regex.IsMatch(str, pattern);
        }
    }
}