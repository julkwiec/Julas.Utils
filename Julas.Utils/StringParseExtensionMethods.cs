﻿using System;

namespace Julas.Utils
{
    public static class StringParseExtensionMethods
    {
        public static Byte ParseByte(this string str)
        {
            return Byte.Parse(str);
        }

        public static Byte ParseByteDef(this string str, Byte fallback)
        {
            Byte value;
            if (Byte.TryParse(str, out value)) return value;
            return fallback;
        }

        public static Byte ParseByteDef(this string str, Func<Byte> fallback)
        {
            Byte value;
            if (Byte.TryParse(str, out value)) return value;
            return fallback();
        }

        public static Byte? ParseByteNull(this string str)
        {
            Byte value;
            if (Byte.TryParse(str, out value)) return value;
            return null;
        }

        public static SByte ParseSByte(this string str)
        {
            return SByte.Parse(str);
        }

        public static SByte ParseSByteDef(this string str, SByte fallback)
        {
            SByte value;
            if (SByte.TryParse(str, out value)) return value;
            return fallback;
        }

        public static SByte ParseSByteDef(this string str, Func<SByte> fallback)
        {
            SByte value;
            if (SByte.TryParse(str, out value)) return value;
            return fallback();
        }

        public static SByte? ParseSByteNull(this string str)
        {
            SByte value;
            if (SByte.TryParse(str, out value)) return value;
            return null;
        }

        public static UInt16 ParseUInt16(this string str)
        {
            return UInt16.Parse(str);
        }

        public static UInt16 ParseUInt16Def(this string str, UInt16 fallback)
        {
            UInt16 value;
            if (UInt16.TryParse(str, out value)) return value;
            return fallback;
        }

        public static UInt16 ParseUInt16Def(this string str, Func<UInt16> fallback)
        {
            UInt16 value;
            if (UInt16.TryParse(str, out value)) return value;
            return fallback();
        }

        public static UInt16? ParseUInt16Null(this string str)
        {
            UInt16 value;
            if (UInt16.TryParse(str, out value)) return value;
            return null;
        }

        public static Int16 ParseInt16(this string str)
        {
            return Int16.Parse(str);
        }

        public static Int16 ParseInt16Def(this string str, Int16 fallback)
        {
            Int16 value;
            if (Int16.TryParse(str, out value)) return value;
            return fallback;
        }

        public static Int16 ParseInt16Def(this string str, Func<Int16> fallback)
        {
            Int16 value;
            if (Int16.TryParse(str, out value)) return value;
            return fallback();
        }

        public static Int16? ParseInt16Null(this string str)
        {
            Int16 value;
            if (Int16.TryParse(str, out value)) return value;
            return null;
        }

        public static UInt32 ParseUInt32(this string str)
        {
            return UInt32.Parse(str);
        }

        public static UInt32 ParseUInt32Def(this string str, UInt32 fallback)
        {
            UInt32 value;
            if (UInt32.TryParse(str, out value)) return value;
            return fallback;
        }

        public static UInt32 ParseUInt32Def(this string str, Func<UInt32> fallback)
        {
            UInt32 value;
            if (UInt32.TryParse(str, out value)) return value;
            return fallback();
        }

        public static UInt32? ParseUInt32Null(this string str)
        {
            UInt32 value;
            if (UInt32.TryParse(str, out value)) return value;
            return null;
        }

        public static Int32 ParseInt32(this string str)
        {
            return Int32.Parse(str);
        }

        public static Int32 ParseInt32Def(this string str, Int32 fallback)
        {
            Int32 value;
            if (Int32.TryParse(str, out value)) return value;
            return fallback;
        }

        public static Int32 ParseInt32Def(this string str, Func<Int32> fallback)
        {
            Int32 value;
            if (Int32.TryParse(str, out value)) return value;
            return fallback();
        }

        public static Int32? ParseInt32Null(this string str)
        {
            Int32 value;
            if (Int32.TryParse(str, out value)) return value;
            return null;
        }

        public static UInt64 ParseUInt64(this string str)
        {
            return UInt64.Parse(str);
        }

        public static UInt64 ParseUInt64Def(this string str, UInt64 fallback)
        {
            UInt64 value;
            if (UInt64.TryParse(str, out value)) return value;
            return fallback;
        }

        public static UInt64 ParseUInt64Def(this string str, Func<UInt64> fallback)
        {
            UInt64 value;
            if (UInt64.TryParse(str, out value)) return value;
            return fallback();
        }

        public static UInt64? ParseUInt64Null(this string str)
        {
            UInt64 value;
            if (UInt64.TryParse(str, out value)) return value;
            return null;
        }

        public static Int64 ParseInt64(this string str)
        {
            return Int64.Parse(str);
        }

        public static Int64 ParseInt64Def(this string str, Int64 fallback)
        {
            Int64 value;
            if (Int64.TryParse(str, out value)) return value;
            return fallback;
        }

        public static Int64 ParseInt64Def(this string str, Func<Int64> fallback)
        {
            Int64 value;
            if (Int64.TryParse(str, out value)) return value;
            return fallback();
        }

        public static Int64? ParseInt64Null(this string str)
        {
            Int64 value;
            if (Int64.TryParse(str, out value)) return value;
            return null;
        }

        public static Boolean ParseBoolean(this string str)
        {
            return Boolean.Parse(str);
        }

        public static Boolean ParseBooleanDef(this string str, Boolean fallback)
        {
            Boolean value;
            if (Boolean.TryParse(str, out value)) return value;
            return fallback;
        }

        public static Boolean ParseBooleanDef(this string str, Func<Boolean> fallback)
        {
            Boolean value;
            if (Boolean.TryParse(str, out value)) return value;
            return fallback();
        }

        public static Boolean? ParseBooleanNull(this string str)
        {
            Boolean value;
            if (Boolean.TryParse(str, out value)) return value;
            return null;
        }
    }
}