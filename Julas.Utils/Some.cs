﻿using System;

namespace Julas.Utils
{
    public struct Some<T>
    {
        public T Value { get; }

        public Some(T value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value), "A Some<T> cannot contain a null value");
            Value = value;
        }

        public static implicit operator T(Some<T> item)
        {
            return item.Value;
        }

        public static implicit operator Some<T>(T value)
        {
            return new Some<T>(value);
        }
    }
}