﻿using Julas.Utils.MathTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Julas.Utils.UnitTest
{
    [TestClass]
    public class MathExtensionMethodsTest
    {
        [TestMethod]
        public void DivideRoundUpPosByPos()
        {
            Assert.AreEqual(5.DivideRoundUp(2), 3);
        }

        [TestMethod]
        public void DivideRoundUpPosByNeg()
        {
            Assert.AreEqual(5.DivideRoundUp(-2), -3);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException), "Division by zero should fail with DivideByZero exception.")]
        public void DivideRoundUpPosByZero()
        {
            5.DivideRoundUp(0);
        }

        [TestMethod]
        public void DivideRoundUpNegByPos()
        {
            Assert.AreEqual(-5.DivideRoundUp(2), -3);
        }

        [TestMethod]
        public void DivideRoundUpNegByNeg()
        {
            Assert.AreEqual(-5.DivideRoundUp(-2), 3);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException), "Division by zero should fail with DivideByZero exception.")]
        public void DivideRoundUpNegByZero()
        {
            (-5).DivideRoundUp(0);
        }

        [TestMethod]
        public void DivideRoundUpZeroByPos()
        {
            Assert.AreEqual(0.DivideRoundUp(2), 0);
        }

        [TestMethod]
        public void DivideRoundUpZeroByNeg()
        {
            Assert.AreEqual(0.DivideRoundUp(-2), 0);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException), "Division by zero should fail with DivideByZero exception.")]
        public void DivideRoundUpZeroByZero()
        {
            0.DivideRoundUp(0);
        }
    }
}