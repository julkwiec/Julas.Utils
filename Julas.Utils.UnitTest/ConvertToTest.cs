﻿using Julas.Utils.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Julas.Utils.UnitTest
{
    [TestClass]
    public class ConvertToTest
    {
        private enum ShortEnum : short
        {
            Value1 = 1,
            Value2 = 2,
            Value3 = 3,
            Value4 = 4,
            Value5 = 5
        }

        private enum IntEnum
        {
            Value1 = 1,
            Value2 = 2,
            Value3 = 3,
            Value4 = 4,
            Value5 = 5
        }

        private void Conversion_OK<TFrom, TTo>(TFrom item, TTo expected)
        {
            TTo result = item.ConvertTo<TTo>();
            Assert.AreEqual(result, expected);
        }

        private void Conversion_Fail<TFrom, TTo>(TFrom item, TTo expected)
        {
            try
            {
                TTo result = item.ConvertTo<TTo>();
                Assert.AreEqual(result, expected);
            }
            catch (Exception)
            {
                return;
            }
            throw new Exception(
                $"Conversion from {typeof(TFrom).FullName} (value: {item.ToStringDef("[null]")}) to {typeof(TTo).FullName} (expected: {expected.ToStringDef("[null]")}) should fail!");
        }

        [TestMethod]
        public void ConvertFromValueType()
        {
            Conversion_OK<short, short>(5, 5);
            Conversion_OK<short, int>(5, 5);
            Conversion_OK<short, short?>(5, 5);
            Conversion_OK<short, int?>(5, 5);
            Conversion_OK<short, ShortEnum>(3, ShortEnum.Value3);
            Conversion_OK<short, IntEnum>(3, IntEnum.Value3);
            Conversion_OK<short, ShortEnum?>(3, ShortEnum.Value3);
            Conversion_OK<short, IntEnum?>(3, IntEnum.Value3);

            Conversion_Fail<short, short>(5, 3);
            Conversion_Fail<short, int>(5, 3);
            Conversion_Fail<short, short?>(5, 3);
            Conversion_Fail<short, int?>(5, 3);
            Conversion_Fail<short, short?>(5, null);
            Conversion_Fail<short, int?>(5, null);
            Conversion_Fail<short, ShortEnum>(3, ShortEnum.Value1);
            Conversion_Fail<short, IntEnum>(3, IntEnum.Value1);
            Conversion_Fail<short, ShortEnum?>(3, ShortEnum.Value1);
            Conversion_Fail<short, IntEnum?>(3, IntEnum.Value1);
            Conversion_Fail<short, ShortEnum?>(3, null);
            Conversion_Fail<short, IntEnum?>(3, null);
        }

        [TestMethod]
        public void ConvertFromEnum()
        {
            Conversion_OK<ShortEnum, short>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum, int>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum, short?>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum, int?>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum, ShortEnum>(ShortEnum.Value3, ShortEnum.Value3);
            Conversion_OK<ShortEnum, IntEnum>(ShortEnum.Value3, IntEnum.Value3);
            Conversion_OK<ShortEnum, ShortEnum?>(ShortEnum.Value3, ShortEnum.Value3);
            Conversion_OK<ShortEnum, IntEnum?>(ShortEnum.Value3, IntEnum.Value3);

            Conversion_Fail<ShortEnum, short>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum, int>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum, short?>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum, int?>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum, short?>(ShortEnum.Value5, null);
            Conversion_Fail<ShortEnum, int?>(ShortEnum.Value5, null);
            Conversion_Fail<ShortEnum, ShortEnum>(ShortEnum.Value3, ShortEnum.Value1);
            Conversion_Fail<ShortEnum, IntEnum>(ShortEnum.Value3, IntEnum.Value1);
            Conversion_Fail<ShortEnum, ShortEnum?>(ShortEnum.Value3, ShortEnum.Value1);
            Conversion_Fail<ShortEnum, IntEnum?>(ShortEnum.Value3, IntEnum.Value1);
            Conversion_Fail<ShortEnum, ShortEnum?>(ShortEnum.Value3, null);
            Conversion_Fail<ShortEnum, IntEnum?>(ShortEnum.Value3, null);
        }

        [TestMethod]
        public void ConvertFromNullableValueType()
        {
            Conversion_OK<short?, short>(5, 5);
            Conversion_OK<short?, int>(5, 5);
            Conversion_OK<short?, short?>(5, 5);
            Conversion_OK<short?, int?>(5, 5);
            Conversion_OK<short?, short?>(null, null);
            Conversion_OK<short?, int?>(null, null);
            Conversion_OK<short?, ShortEnum>(3, ShortEnum.Value3);
            Conversion_OK<short?, IntEnum>(3, IntEnum.Value3);
            Conversion_OK<short?, ShortEnum?>(3, ShortEnum.Value3);
            Conversion_OK<short?, IntEnum?>(3, IntEnum.Value3);
            Conversion_OK<short?, ShortEnum?>(null, null);
            Conversion_OK<short?, IntEnum?>(null, null);

            Conversion_Fail<short?, short>(5, 3);
            Conversion_Fail<short?, int>(5, 3);
            Conversion_Fail<short?, short?>(5, 3);
            Conversion_Fail<short?, int?>(5, 3);
            Conversion_Fail<short?, short?>(null, 3);
            Conversion_Fail<short?, int?>(null, 3);
            Conversion_Fail<short?, short?>(3, null);
            Conversion_Fail<short?, int?>(3, null);
            Conversion_Fail<short?, ShortEnum>(3, ShortEnum.Value1);
            Conversion_Fail<short?, IntEnum>(3, IntEnum.Value1);
            Conversion_Fail<short?, ShortEnum?>(3, ShortEnum.Value1);
            Conversion_Fail<short?, IntEnum?>(3, IntEnum.Value1);
            Conversion_Fail<short?, ShortEnum?>(3, null);
            Conversion_Fail<short?, IntEnum?>(3, null);
            Conversion_Fail<short?, ShortEnum?>(null, ShortEnum.Value1);
            Conversion_Fail<short?, IntEnum?>(null, IntEnum.Value1);
        }

        [TestMethod]
        public void ConvertFromNullableEnum()
        {
            Conversion_OK<ShortEnum?, short>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum?, int>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum?, short?>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum?, int?>(ShortEnum.Value5, 5);
            Conversion_OK<ShortEnum?, short?>(null, null);
            Conversion_OK<ShortEnum?, int?>(null, null);
            Conversion_OK<ShortEnum?, ShortEnum>(ShortEnum.Value3, ShortEnum.Value3);
            Conversion_OK<ShortEnum?, IntEnum>(ShortEnum.Value3, IntEnum.Value3);
            Conversion_OK<ShortEnum?, ShortEnum?>(ShortEnum.Value3, ShortEnum.Value3);
            Conversion_OK<ShortEnum?, IntEnum?>(ShortEnum.Value3, IntEnum.Value3);
            Conversion_OK<ShortEnum?, ShortEnum?>(null, null);
            Conversion_OK<ShortEnum?, IntEnum?>(null, null);

            Conversion_Fail<ShortEnum?, short>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum?, int>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum?, short?>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum?, int?>(ShortEnum.Value5, 3);
            Conversion_Fail<ShortEnum?, short?>(null, 3);
            Conversion_Fail<ShortEnum?, int?>(null, 3);
            Conversion_Fail<ShortEnum?, short?>(ShortEnum.Value3, null);
            Conversion_Fail<ShortEnum?, int?>(ShortEnum.Value3, null);
            Conversion_Fail<ShortEnum?, ShortEnum>(ShortEnum.Value3, ShortEnum.Value1);
            Conversion_Fail<ShortEnum?, IntEnum>(ShortEnum.Value3, IntEnum.Value1);
            Conversion_Fail<ShortEnum?, ShortEnum?>(ShortEnum.Value3, ShortEnum.Value1);
            Conversion_Fail<ShortEnum?, IntEnum?>(ShortEnum.Value3, IntEnum.Value1);
            Conversion_Fail<ShortEnum?, ShortEnum?>(ShortEnum.Value3, null);
            Conversion_Fail<ShortEnum?, IntEnum?>(ShortEnum.Value3, null);
            Conversion_Fail<ShortEnum?, ShortEnum?>(null, ShortEnum.Value1);
            Conversion_Fail<ShortEnum?, IntEnum?>(null, IntEnum.Value1);
        }
    }
}