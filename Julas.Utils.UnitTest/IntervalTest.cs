﻿using Julas.Utils.MathTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Julas.Utils.UnitTest
{
    [TestClass]
    public class IntervalTest
    {
        private void CheckNew(int lower, int upper, int expectedCount)
        {
            var interval1 = new Interval(lower, upper);
            var interval2 = new Interval(upper, lower);

            Assert.AreEqual(interval1.Count, expectedCount);
            Assert.AreEqual(interval2.Count, expectedCount);
            Assert.AreEqual(interval1.LowerBound, Math.Min(lower, upper));
            Assert.AreEqual(interval2.LowerBound, Math.Min(lower, upper));
            Assert.AreEqual(interval1.UpperBound, Math.Max(lower, upper));
            Assert.AreEqual(interval2.UpperBound, Math.Max(lower, upper));
        }

        private void CheckOverlaps(Interval interval1, Interval interval2, bool expextedResult)
        {
            Assert.AreEqual(interval1.Overlaps(interval2), expextedResult);
            Assert.AreEqual(interval2.Overlaps(interval1), expextedResult);
        }

        private void CheckContains(Interval interval1, Interval interval2, bool expextedResult)
        {
            Assert.AreEqual(interval1.Contains(interval2), expextedResult);
        }

        private void CheckEquals(Interval interval1, Interval interval2, bool expectedResult)
        {
            Assert.AreEqual(interval1.Equals(interval2), expectedResult);
        }

        [TestMethod]
        public void NewValue()
        {
            CheckNew(-10, -10, 1);
            CheckNew(-10, -1, 10);
            CheckNew(-10, 0, 11);
            CheckNew(-10, 1, 12);
            CheckNew(-10, 10, 21);

            CheckNew(-1, -10, 10);
            CheckNew(-1, -1, 1);
            CheckNew(-1, 0, 2);
            CheckNew(-1, 1, 3);
            CheckNew(-1, 10, 12);

            CheckNew(0, -10, 11);
            CheckNew(0, -1, 2);
            CheckNew(0, 0, 1);
            CheckNew(0, 1, 2);
            CheckNew(0, 10, 11);

            CheckNew(1, -10, 12);
            CheckNew(1, -1, 3);
            CheckNew(1, 0, 2);
            CheckNew(1, 1, 1);
            CheckNew(1, 10, 10);

            CheckNew(10, -10, 21);
            CheckNew(10, -1, 12);
            CheckNew(10, 0, 11);
            CheckNew(10, 1, 10);
            CheckNew(10, 10, 1);
        }

        [TestMethod]
        public void Equals()
        {
            var interval1 = new Interval(-10, -1);
            var interval2 = new Interval(-5, -4);
            var interval3 = new Interval(-6, 0);
            var interval4 = new Interval(-1, 5);
            var interval5 = new Interval(2, 8);
            var interval6 = new Interval(6, 10);

            CheckEquals(interval1, interval1, true);
            CheckEquals(interval1, interval2, false);
            CheckEquals(interval1, interval3, false);
            CheckEquals(interval1, interval4, false);
            CheckEquals(interval1, interval5, false);
            CheckEquals(interval1, interval6, false);

            CheckEquals(interval2, interval2, true);
            CheckEquals(interval2, interval3, false);
            CheckEquals(interval2, interval4, false);
            CheckEquals(interval2, interval5, false);
            CheckEquals(interval2, interval6, false);

            CheckEquals(interval3, interval3, true);
            CheckEquals(interval3, interval4, false);
            CheckEquals(interval3, interval5, false);
            CheckEquals(interval3, interval6, false);

            CheckEquals(interval4, interval4, true);
            CheckEquals(interval4, interval5, false);
            CheckEquals(interval4, interval6, false);

            CheckEquals(interval5, interval5, true);
            CheckEquals(interval5, interval6, false);

            CheckEquals(interval6, interval6, true);
        }

        [TestMethod]
        public void Overlaps()
        {
            var interval1 = new Interval(-10, -1);
            var interval2 = new Interval(-5, -4);
            var interval3 = new Interval(-6, 0);
            var interval4 = new Interval(-1, 5);
            var interval5 = new Interval(2, 8);
            var interval6 = new Interval(6, 10);

            CheckOverlaps(interval1, interval1, true);
            CheckOverlaps(interval1, interval2, true);
            CheckOverlaps(interval1, interval3, true);
            CheckOverlaps(interval1, interval4, true);
            CheckOverlaps(interval1, interval5, false);
            CheckOverlaps(interval1, interval6, false);

            CheckOverlaps(interval2, interval2, true);
            CheckOverlaps(interval2, interval3, true);
            CheckOverlaps(interval2, interval4, false);
            CheckOverlaps(interval2, interval5, false);
            CheckOverlaps(interval2, interval6, false);

            CheckOverlaps(interval3, interval3, true);
            CheckOverlaps(interval3, interval4, true);
            CheckOverlaps(interval3, interval5, false);
            CheckOverlaps(interval3, interval6, false);

            CheckOverlaps(interval4, interval4, true);
            CheckOverlaps(interval4, interval5, true);
            CheckOverlaps(interval4, interval6, false);

            CheckOverlaps(interval5, interval5, true);
            CheckOverlaps(interval5, interval6, true);

            CheckOverlaps(interval6, interval6, true);
        }

        [TestMethod]
        public void Contains()
        {
            var interval1 = new Interval(-10, -1);
            var interval2 = new Interval(-5, -4);
            var interval3 = new Interval(-6, 0);
            var interval4 = new Interval(-1, 5);
            var interval5 = new Interval(2, 8);
            var interval6 = new Interval(6, 10);

            CheckContains(interval1, interval1, true);
            CheckContains(interval1, interval2, true);
            CheckContains(interval1, interval3, false);
            CheckContains(interval1, interval4, false);
            CheckContains(interval1, interval5, false);
            CheckContains(interval1, interval6, false);

            CheckContains(interval2, interval1, false);
            CheckContains(interval2, interval2, true);
            CheckContains(interval2, interval3, false);
            CheckContains(interval2, interval4, false);
            CheckContains(interval2, interval5, false);
            CheckContains(interval2, interval6, false);

            CheckContains(interval3, interval1, false);
            CheckContains(interval3, interval2, true);
            CheckContains(interval3, interval3, true);
            CheckContains(interval3, interval4, false);
            CheckContains(interval3, interval5, false);
            CheckContains(interval3, interval6, false);

            CheckContains(interval4, interval1, false);
            CheckContains(interval4, interval2, false);
            CheckContains(interval4, interval3, false);
            CheckContains(interval4, interval4, true);
            CheckContains(interval4, interval5, false);
            CheckContains(interval4, interval6, false);

            CheckContains(interval5, interval1, false);
            CheckContains(interval5, interval2, false);
            CheckContains(interval5, interval3, false);
            CheckContains(interval5, interval4, false);
            CheckContains(interval5, interval5, true);
            CheckContains(interval5, interval6, false);

            CheckContains(interval6, interval1, false);
            CheckContains(interval6, interval2, false);
            CheckContains(interval6, interval3, false);
            CheckContains(interval6, interval4, false);
            CheckContains(interval6, interval5, false);
            CheckContains(interval6, interval6, true);
        }
    }
}