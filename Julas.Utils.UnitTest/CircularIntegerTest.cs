﻿using Julas.Utils.MathTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Julas.Utils.UnitTest
{
    [TestClass]
    public class CircularIntegerTest
    {
        [TestMethod]
        public void GetDistance()
        {
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(1, 10), 1, 2), 1);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(1, 10), 2, 1), 1);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(1, 10), 2, 9), 3);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(1, 10), 9, 2), 3);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(-10, 10), 1, 2), 1);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(-10, 10), 2, 1), 1);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(-10, 10), -10, -9), 1);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(-10, 10), -9, -10), 1);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(-10, 10), -10, 9), 2);
            Assert.AreEqual(CircularInteger.GetDistance(new Interval(-10, 10), 9, -10), 2);
        }

        [TestMethod]
        public void NewValue()
        {
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), 0).Value, 0);
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), 1).Value, 1);
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), 8).Value, 8);
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), 9).Value, 9);
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), 10).Value, 0);
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), 11).Value, 1);
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), -1).Value, 9);
            Assert.AreEqual(new CircularInteger(new Interval(0, 9), -2).Value, 8);
        }

        [TestMethod]
        public void Addition()
        {
            var cint = new CircularInteger(new Interval(-3, 3), 0);
            Assert.AreEqual(cint.Value, 0);

            cint++;
            Assert.AreEqual(cint.Value, 1);
            cint++;
            Assert.AreEqual(cint.Value, 2);
            cint++;
            Assert.AreEqual(cint.Value, 3);
            cint++;
            Assert.AreEqual(cint.Value, -3);
            cint++;
            Assert.AreEqual(cint.Value, -2);
            cint++;
            Assert.AreEqual(cint.Value, -1);
            cint++;
            Assert.AreEqual(cint.Value, 0);

            cint += 7;
            Assert.AreEqual(cint.Value, 0);
            cint += 2;
            Assert.AreEqual(cint.Value, 2);
        }

        [TestMethod]
        public void Substraction()
        {
            var cint = new CircularInteger(new Interval(-3, 3), 0);
            Assert.AreEqual(cint.Value, 0);

            cint--;
            Assert.AreEqual(cint.Value, -1);
            cint--;
            Assert.AreEqual(cint.Value, -2);
            cint--;
            Assert.AreEqual(cint.Value, -3);
            cint--;
            Assert.AreEqual(cint.Value, 3);
            cint--;
            Assert.AreEqual(cint.Value, 2);
            cint--;
            Assert.AreEqual(cint.Value, 1);
            cint--;
            Assert.AreEqual(cint.Value, 0);

            cint -= 7;
            Assert.AreEqual(cint.Value, 0);
            cint -= 2;
            Assert.AreEqual(cint.Value, -2);
        }
    }
}