﻿using Julas.Utils.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Julas.Utils.UnitTest
{
    [TestClass]
    public class CollectionExtensionMethodsTest
    {
        [TestMethod]
        public void LoopTest()
        {
            var sequence = Enumerable.Range(1, 5).Repeat().Skip(2).Take(10).ToArray();
            var shouldBe = new[] { 3, 4, 5, 1, 2, 3, 4, 5, 1, 2 };
            CollectionAssert.AreEqual(sequence, shouldBe);
        }

        [TestMethod]
        public void RepeatTest()
        {
            var sequence = Enumerable.Range(1, 5).Repeat(0).ToArray();
            CollectionAssert.AreEqual(sequence, new int[] { });

            sequence = Enumerable.Range(1, 5).Repeat(1).ToArray();
            CollectionAssert.AreEqual(sequence, new int[] { 1, 2, 3, 4, 5 });

            sequence = Enumerable.Range(1, 5).Repeat(2).ToArray();
            CollectionAssert.AreEqual(sequence, new int[] { 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 });
        }

        [TestMethod]
        public void IsNullOrEmptyTest()
        {
            int[] collection1 = null;
            int[] collection2 = new int[0];
            int[] collection3 = new int[5];
            Assert.IsTrue(collection1.IsNullOrEmpty());
            Assert.IsTrue(collection2.IsNullOrEmpty());
            Assert.IsFalse(collection3.IsNullOrEmpty());
        }
    }
}