﻿using Julas.Utils.Reactive.Properties;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Julas.Utils.Reactive")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Julas.Utils.Reactive")]
[assembly: AssemblyCopyright("Copyright © Julian Kwieciński 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("02f2fbcb-cf56-4aa0-a4bd-ebf0f7edff70")]
[assembly: AssemblyVersion(AssemblyInfo.VersionNumber)]
[assembly: AssemblyFileVersion(AssemblyInfo.VersionNumber)]
[assembly: AssemblyInformationalVersion(AssemblyInfo.VersionNumber)]
[assembly: InternalsVisibleTo("Julas.Utils.Reactive.UnitTest")]

namespace Julas.Utils.Reactive.Properties
{
    internal static class AssemblyInfo
    {
        internal const string VersionNumber = "1.0.1.0";
    }
}