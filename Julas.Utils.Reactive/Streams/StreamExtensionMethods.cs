﻿using System.IO;
using System.Threading.Tasks;

namespace Julas.Utils.Reactive
{
    public static class StreamExtensionMethods
    {
        public static async Task<int> ReadByteAsync(this Stream stream)
        {
            var b = new byte[1];
            var bytesRead = await stream.ReadAsync(b, 0, 1);
            return bytesRead == 0 ? -1 : b[0];
        }
    }
}