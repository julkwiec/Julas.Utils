﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Julas.Utils.Extensions;

namespace Julas.Utils.Streams
{
    public interface IStreamInterface : ICancelable
    {
        Task<ReadResult> ReadAsync(int count);
        ReadResult Read(int count);
        Task<Option<byte>> ReadByteAsync();
        Option<byte> ReadByte();
        void Write(byte[] data);
        Task<Unit> WriteAsync(byte[] data);
    }
}
