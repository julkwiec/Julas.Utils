using System.IO;

namespace Julas.Utils.Streams
{
    public static class StreamInterface
    {
        public static IStreamInterface ToStreamInterface(this Stream stream)
        {
            return new StreamInterfaceBase(stream);
        }

        public static IStreamInterface FromStream(Stream stream)
        {
            return new StreamInterfaceBase(stream);
        }
    }
}