﻿namespace Julas.Utils.Reactive
{
    public struct ReadResult
    {
        public byte[] Buffer { get; private set; }
        public int BytesRead { get; private set; }

        public ReadResult(byte[] buffer, int bytesRead)
        {
            Buffer = buffer;
            BytesRead = bytesRead;
        }
    }
}