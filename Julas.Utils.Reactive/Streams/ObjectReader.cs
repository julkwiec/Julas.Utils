using System;
using System.IO;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Julas.Utils.Reactive
{
    public static class ObjectReader
    {
        public static IObjectReader<T> Create<T>(Stream stream, Func<Stream, Task<Option<T>>> readNextAsync)
        {
            return new ObjectReaderBase<T>(stream, readNextAsync);
        }

        public static IObservable<T> ToObservable<T>(this IObjectReader<T> reader)
        {
            return
                Observable.FromAsync(reader.ReadAsync)
                    .Repeat()
                    .TakeWhile(x => x.HasValue)
                    .Select(x => x.Value)
                    .Catch<T, ObjectDisposedException>(ex => Observable.Empty<T>());
        }
    }
}