﻿using Julas.Utils.Extensions;
using System;
using System.IO;
using System.Reactive;
using System.Threading.Tasks;

namespace Julas.Utils.Reactive
{
    internal class ObjectWriterBase<T> : CancelableBase, IObjectWriter<T>
    {
        public Stream Stream { get; private set; }
        private readonly Func<Stream, T, Task<Unit>> _WriteAsync;

        public ObjectWriterBase(Stream stream, Func<Stream, T, Task<Unit>> writeAsync)
        {
            Stream = stream;
            _WriteAsync = writeAsync;
        }

        public void Write(T message)
        {
            WriteAsync(message).WaitThrow();
        }

        public async Task<Unit> WriteAsync(T message)
        {
            ThrowIfDisposed();
            return await _WriteAsync(Stream, message);
        }

        protected override void Dispose(bool disposing)
        {
            Stream.Dispose();
        }
    }
}