using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;

namespace Julas.Utils.Reactive
{
    public interface IObjectWriter<T> : ICancelable
    {
        void Write(T message);

        Task<Unit> WriteAsync(T message);

        Stream Stream { get; }
    }
}