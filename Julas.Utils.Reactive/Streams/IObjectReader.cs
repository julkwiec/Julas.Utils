using System.IO;
using System.Reactive.Disposables;
using System.Threading.Tasks;

namespace Julas.Utils.Reactive
{
    public interface IObjectReader<T> : ICancelable
    {
        Option<T> Read();

        Task<Option<T>> ReadAsync();

        Stream Stream { get; }
    }
}