﻿using System.IO;
using System.Reactive;
using System.Threading.Tasks;

namespace Julas.Utils.Streams
{
    internal class StreamInterfaceBase : DisposableBase, IStreamInterface
    {
        private readonly Stream _Stream;

        public StreamInterfaceBase(Stream stream)
        {
            _Stream = stream;
        }

        protected override void DoDispose()
        {
            _Stream.Close();
        }

        public async Task<ReadResult> ReadAsync(int count)
        {
            ThrowIfDisposed();
            var buffer = new byte[count];
            var bytesRead = await _Stream.ReadAsync(buffer, 0, count);
            return new ReadResult(buffer, bytesRead);
        }

        public ReadResult Read(int count)
        {
            ThrowIfDisposed();
            var buffer = new byte[count];
            var bytesRead = _Stream.Read(buffer, 0, count);
            return new ReadResult(buffer, bytesRead);
        }

        public async Task<Option<byte>> ReadByteAsync()
        {
            ThrowIfDisposed();
            var buffer = new byte[1];
            var bytesRead = await _Stream.ReadAsync(buffer, 0, 1);
            if (bytesRead == 0) return Option<byte>.None;
            return buffer[0];
        }

        public Option<byte> ReadByte()
        {
            ThrowIfDisposed();
            var value = _Stream.ReadByte();
            return value >= 0 ? (byte) value : Option<byte>.None;
        }

        public void Write(byte[] data)
        {
            ThrowIfDisposed();
            _Stream.Write(data, 0, data.Length);
        }

        public async Task<Unit> WriteAsync(byte[] data)
        {
            ThrowIfDisposed();
            await _Stream.WriteAsync(data, 0, data.Length);
            return Unit.Default;
        }
    }
}