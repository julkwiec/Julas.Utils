﻿using System;
using System.IO;
using System.Reactive;
using System.Threading.Tasks;

namespace Julas.Utils.Reactive
{
    public static class ObjectWriter
    {
        public static IObjectWriter<T> Create<T>(Stream stream, Func<Stream, T, Task<Unit>> writeAsync)
        {
            return new ObjectWriterBase<T>(stream, writeAsync);
        }
    }
}