using Julas.Utils.Extensions;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Julas.Utils.Reactive
{
    internal class ObjectReaderBase<T> : CancelableBase, IObjectReader<T>
    {
        public Stream Stream { get; private set; }
        private readonly Func<Stream, Task<Option<T>>> _ReadNextAsync;

        public ObjectReaderBase(Stream stream, Func<Stream, Task<Option<T>>> readNextAsync)
        {
            Stream = stream;
            _ReadNextAsync = readNextAsync;
        }

        public Option<T> Read()
        {
            return ReadAsync().WaitThrow();
        }

        public async Task<Option<T>> ReadAsync()
        {
            ThrowIfDisposed();
            return await _ReadNextAsync(Stream);
        }

        protected override void Dispose(bool disposing)
        {
            Stream.Dispose();
        }
    }
}