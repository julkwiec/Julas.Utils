﻿using System;
using System.Net.Sockets;
using System.Reactive.Linq;

namespace Julas.Utils.Reactive
{
    public static class SocketExtensions
    {
        /// <summary>
        ///     Wraps a TcpListener in an observable. On subscription the listener starts emitting TcpClients. If the listener is
        ///     closed gracefully (i.e. disposed), the sequence ends with OnCompleted.
        /// </summary>
        /// <param name="listener">A listener to wrap</param>
        /// <returns>An observabl sequence of connected TcpClients</returns>
        public static IObservable<TcpClient> ObserveClients(this TcpListener listener)
        {
            return Observable
                .FromAsync(listener.AcceptTcpClientAsync)
                .Repeat()
                .Catch<TcpClient, ObjectDisposedException>(ex => Observable.Empty<TcpClient>());
        }
    }
}