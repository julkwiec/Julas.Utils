﻿using System.Reactive.Disposables;

namespace Julas.Utils.Reactive
{
    public abstract class CancelableBase : DisposableBase, ICancelable
    {
        public new bool IsDisposed => base.IsDisposed;
    }
}