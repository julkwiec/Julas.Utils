# Julas.Utils

This code is available as a NuGet package: https://www.nuget.org/packages/Julas.Utils/

Example usage of observable TCP connections:

```C#
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Julas.Utils;
using Julas.Utils.Connections;
using Julas.Utils.Extensions;
using Julas.Utils.Sockets;

namespace service
{
    class Program
    {
        static void Main(string[] args)
        {
            var endpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 14001);
            var src = JsonTcpConnectionSource.Create<dynamic>(endpoint);
            src.Subscribe(
                con =>
                {
                    Console.WriteLine("[EchoServer] Connection accepted");
                    con.Subscribe(
                        msg =>
                        {
                            Console.WriteLine("[EchoServer] Message received: " + msg.Value);
                            msg.Value = "ECHO: " + msg.Value;
                            Console.WriteLine("[EchoServer] Sending response: " + msg.Value);
                            con.Send(msg);
                        },
                        ex =>
                        {
                            Console.WriteLine("[EchoServer] Connection error: " + ex.Message);
                        },
                        () =>
                        {
                            Console.WriteLine("[EchoServer] Connection closed");
                        });
                },
                ex =>
                {
                    Console.WriteLine("[EchoServer] ConnectionSource error: " + ex.Message);
                }, 
                () =>
                {
                    Console.WriteLine("[EchoServer] ConnectionSource closed");
                });
            Console.WriteLine("Press a key to start...");
            Console.ReadKey();
            Spam(endpoint);
            Console.WriteLine("Press a key to close connections...");
            Console.ReadKey();
            src.Dispose();
            Console.WriteLine("Press a key to finish...");
            Console.ReadKey();
        }

        private static void Spam(IPEndPoint endpoint)
        {
            var con = JsonTcpConnection.Create<dynamic>(endpoint);
            con.Subscribe(
                msg =>
                {
                    Console.WriteLine("[Client] Message received: " + msg.Value);
                },
                ex => 
                {
                    Console.WriteLine("[Client] Connection error: " + ex.Message);
                },
                () =>
                {
                    Console.WriteLine("[Client] Connection closed");
                });

            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    try
                    {
                        dynamic msg = new ExpandoObject();
                        msg.Value = "SomeText";
                        con.Send(msg);
                        Console.WriteLine("[Client] Sendt message: " + msg.Value);
                        Task.Delay(1000).Wait();
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
            });
        }
    }
}

```
